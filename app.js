const express = require('express');
const bodyParser = require('body-parser');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const cors = require('cors');

const app = express();
const port = 3001;

app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true,
}));

app.use(bodyParser.json());

// Swagger configuration
const swaggerDefinition = {
    info: {
        title: 'TodoList API',
        version: '1.0.0',
        description: 'API for managing TodoList tasks',
    },
    basePath: '/',
};

const options = {
    swaggerDefinition,
    apis: ['./app.js'], // Point to the file containing your routes
};

const swaggerSpec = swaggerJSDoc(options);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Data file path
const dataFilePath = 'tasks.json';

// Read tasks from the data file
const readTasks = () => {
    try {
        const data = fs.readFileSync(dataFilePath);
        return JSON.parse(data);
    } catch (error) {
        return [];
    }
};

// Write tasks to the data file
const writeTasks = (tasks) => {
    fs.writeFileSync(dataFilePath, JSON.stringify(tasks, null, 2));
};

/**
 * @swagger
 * /todos:
 *   get:
 *     description: Get all TodoList tasks
 *     responses:
 *       200:
 *         description: Successful response
 */
app.get('/todos', (req, res) => {
    const tasks = readTasks();
    res.json(tasks);
});

/**
 * @swagger
 * /todos:
 *   post:
 *     description: Create a new TodoList task
 *     parameters:
 *       - name: task
 *         description: Task object
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *     responses:
 *       200:
 *         description: Successful response
 */
app.post('/todos', (req, res) => {
    console.log(req);
    const tasks = readTasks();
    const newTask = req.body;
    newTask.done = false;
    newTask.editing = false;

    const lastTask = tasks.slice(-1).pop();
    if (tasks.length != 0) {
        newTask.id = lastTask.id + 1;
    } else {
        newTask.id = 1;
    }

    tasks.push(newTask);
    writeTasks(tasks);
    res.json(newTask);
});

/**
 * @swagger
 * /todos/{id}:
 *   put:
 *     description: Update a TodoList task
 *     parameters:
 *       - name: id
 *         description: Task ID
 *         in: path
 *         required: true
 *         type: string
 *       - name: task
 *         description: Updated task object
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             done:
 *               type: boolean
 *             editing:
 *               type: boolean
 *     responses:
 *       200:
 *         description: Successful response
 */
app.put('/todos/:id', (req, res) => {
    const tasks = readTasks();
    console.log(req.params.id);
    const taskId = req.params.id;
    const updatedTask = req.body;

    // Find the task by ID and update it
    const index = tasks.findIndex((task) => Number(task.id) === Number(taskId));
    if (index !== -1) {
        tasks[index] = { ...tasks[index], ...updatedTask };
        writeTasks(tasks);
        res.json(tasks[index]);
    } else {
        res.status(404).json({ error: 'Task not found' });
    }
});

/**
 * @swagger
 * /todos/{id}:
 *   delete:
 *     description: Delete a TodoList task
 *     parameters:
 *       - name: id
 *         description: Task ID
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successful response
 */
app.delete('/todos/:id', (req, res) => {
    const tasks = readTasks();
    const taskId = req.params.id;

    // Find the task by ID and remove it
    const filteredTasks = tasks.filter((task) => Number(task.id) !== Number(taskId));

    if (tasks.length !== filteredTasks.length) {
        writeTasks(filteredTasks);
        res.json({ success: true });
    } else {
        res.status(404).json({ error: 'Task not found' });
    }
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
